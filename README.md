Welcome Earthling!

# About

This is the proaudio gentoo overlay containing proaudio software and libs
that are not in official portage.
The ebuilds come from different sources: some of them are written by gimpel,
lots of them are from evermind's superb pro-audio overlay which can be found
in the [gentoo-forum](http://forums.gentoo.org/viewtopic-t-427211.html).

## About this repository

We migrate the original SVN project to Git right now.
But you should be save to use this repository over the SVN.

# Disclaimer:

Parts of this overlay contain highly experimental software. USE WITH CARE!
Do NOT blame us or even worse the gentoo developers if using these ebuilds
causes your computer to explode, your cat violently attacs you, your girlfriend
tells you to STFUAPOA or god knows what else.

# Usage:

Add the repository by put a file `proaudio` in `/etc/portage/repos.conf` with a content like this

    [proaudio]
    location = /usr/local/overlays/proaudio/
    sync-type = git
    sync-uri = https://gitlab.com/proaudio/proaudio.git
    auto-sync = yes

For info on how to do so, please refer to
[gentoo wiki](https://wiki.gentoo.org/wiki//etc/portage/repos.conf).

# Trouble?

From time to time it might happen that you get things like "digest verification failed" or similar.
This is because someone forgot to re-digest the ebuild after commiting changes. Just cd in the
directory of the ebuild ("foo" in the example) and do:

    $ ebuild foo.ebuild digest

# Report bugs

## via GitLab Issues (preferred)

just write a GitLab Issue using the subject pattern `<full-ebuild-name> <version> <issue-text>`

## via Mailing-List

you can send bugs, questions, contributions to our
[Mailing-List](mailto:proaudio@lists.tuxfamily.org)
you need to be subscibed to post messages.

### subscribe to Mailing-List
send a email to
[proaudio-request@lists.tuxfamily.org](proaudio-request@lists.tuxfamily.org)
with subject: `subscribe`

### unsubscribe from Mailing-List
same as above but subject: `unsubscribe`


# Further Information:

Notes about some provided ebuilds
read: 00-OVERLAY-INFO
