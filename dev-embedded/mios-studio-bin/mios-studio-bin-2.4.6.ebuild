# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

MULTILIB_COMPAT=( abi_x86_32 )
KEYWORDS="-* ~amd64 ~x86"
inherit eutils multilib-build

DESCRIPTION="uCApps.de MIOS Studio"
SRC_URI="http://ucapps.de/mios_studio/MIOS_Studio_2_4_6.tar.gz"
HOMEPAGE="http://ucapps.de/"

LICENSE="TAPR-NCL"
SLOT="0"
IUSE=""

RESTRICT="strip mirror"

DEPEND=""

RDEPEND="app-arch/bzip2[${MULTILIB_USEDEP}]
	media-libs/alsa-lib[${MULTILIB_USEDEP}]
	media-libs/freetype[${MULTILIB_USEDEP}]
	media-libs/libpng:0[${MULTILIB_USEDEP}]
	sys-libs/zlib[${MULTILIB_USEDEP}]
	x11-libs/libX11[${MULTILIB_USEDEP}]
	x11-libs/libXau[${MULTILIB_USEDEP}]
	x11-libs/libXdmcp[${MULTILIB_USEDEP}]
	x11-libs/libXext[${MULTILIB_USEDEP}]
	x11-libs/libxcb[${MULTILIB_USEDEP}]
"

INSTALLDIR=/opt

S="${WORKDIR}"

src_install() {
	exeinto /opt/mios-studio-${PV}/bin
	doexe MIOS_Studio
	dosym /opt/mios-studio-${PV}/bin/MIOS_Studio /opt/bin/MIOS_Studio
}
