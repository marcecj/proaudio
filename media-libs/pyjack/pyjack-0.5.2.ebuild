# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

PYTHON_COMPAT=( python2_7 )
inherit distutils-r1

DESCRIPTION="jack audio client module for python"
HOMEPAGE="http://py-jack.sourceforge.net/"
SRC_URI="http://downloads.sourceforge.net/project/py-jack/py-jack/${PV}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="examples"

DEPEND="media-sound/jack-audio-connection-kit"
RDEPEND="${DEPEND}"

src_install() {
	default

	if use "examples"; then
		docinto examples
		dodoc demos/*
	fi
}
