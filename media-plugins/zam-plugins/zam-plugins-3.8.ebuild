# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Collection of LV2/LADSPA audio plugins for high quality processing"
HOMEPAGE="http://www.zamaudio.com/?p=870 https://github.com/zamaudio/zam-plugins"
# snapshot of git repo with pulled submodules copied to proaudio distfiles
SRC_URI="https://github.com/zamaudio/${PN}/archive/${PV}.tar.gz
	https://github.com/DISTRHO/DPF/archive/bb29962a512a8911860de2b9b1da468b3f771538.zip -> dpf-${P}.zip"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="media-libs/liblo
	media-libs/mesa
	x11-libs/libX11
	virtual/jack
"
DEPEND="${RDEPEND}
	media-libs/ladspa-sdk
	media-libs/lv2
	virtual/pkgconfig"

src_prepare() {
	default

	# The build system expects a local copy of dpf (normally a git submodule,
	# but this is using the regular source archives).  Use globs here to be
	# independent of the particular snapshot used.
	mv "${WORKDIR}"/DPF-*/* "${S}/dpf/"
}
