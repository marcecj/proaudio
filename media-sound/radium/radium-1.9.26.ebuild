# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

PYTHON_COMPAT=( python2_7 )
inherit eutils multilib python-r1

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RESTRICT="mirror"
DESCRIPTION="A music editor with a new type of interface."
HOMEPAGE="http://users.notam02.no/~kjetism/${PN}/"
SRC_URI="http://archive.notam02.no/arkiv/src/${P}.tar.gz"

KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE="debug"
LICENSE="GPL-2"

DEPEND="dev-qt/qtcore[qt3support]
	x11-libs/libXaw
	media-libs/alsa-lib
	media-sound/jack-audio-connection-kit
	media-libs/libsamplerate
	media-libs/liblrdf
	media-libs/libsndfile
	media-libs/ladspa-sdk
	>=dev-libs/glib-2.0"

#src_prepare() {
#	sed -i -e 's:DUSE_VESTIGE=0:DUSE_VESTIGE=1:' "${S}/Makefile.Qt" || die "sed failed"
#}

src_compile() {
	if use debug; then
		BUILDT=DEBUG
	else
		BUILDT=RELEASE
	fi
	emake DESTDIR="${D}" PREFIX="/usr" libdir="/usr/$(get_libdir)" \
		OPTIMIZE="${CXXFLAGS}" packages
	BUILDTYPE="${BUILDT}" ./build_linux.sh -j7 || die "Build failed"
}

src_install() {
	emake libdir="/usr/$(get_libdir)" DESTDIR="${D}" PREFIX="/usr" install
	insinto /usr/share/pixmaps
	doins "${FILESDIR}/radium.xpm"
	make_desktop_entry radium Radium "radium" "AudioVideo;Audio;AudioVideoEditing;"
}
