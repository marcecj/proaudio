# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

inherit eutils toolchain-funcs

DESCRIPTION="Yet Another Scrolling Scope"
HOMEPAGE="http://kokkinizita.linuxaudio.org/linuxaudio/"
SRC_URI="http://kokkinizita.linuxaudio.org/linuxaudio/downloads/${P}.tar.bz2"

RESTRICT="mirror"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=">=media-libs/libclthreads-2.2.1
	>=media-libs/libclxclient-3.3.1
	x11-libs/libX11
	x11-libs/libXft
	virtual/jack"
RDEPEND="${RDEPEND}"

DOCS=( AUTHORS README )

PATCHES=( "${FILESDIR}/${P}-makefile.patch" )

S="${WORKDIR}/${PN}"

src_compile() {
	tc-export CXX
	emake PREFIX="${EPREFIX}usr"
}
